import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayoutTabs.vue'),
    children: [
      { path: '', component: () => import('pages/IndexPage.vue') },
      { path: '/about', name: 'about', component: () => import('pages/AboutPage.vue') },
      { path: '/typography', name: 'typography', component: () => import('pages/TypographyPage.vue') },
      { path: '/flex', name: 'flex', component: () => import('pages/FlexPage.vue') },
      { path: '/logistic', name: 'logistic', component: () => import('pages/logistic/supplier/SupplierLogistic.vue') },
      { path: '/ordenCompra', name: 'ordenCompra', component: () => import('pages/logistic/ordenCompra/OrdenCompra.vue') },
      { path: '/almacen', name: 'almacen', component: () => import('pages/logistic/almacen/AlmacenLogistic.vue') },
      { path: '/producto', name: 'producto', component: () => import('pages/logistic/producto/SupplierProduct.vue') },
      //ACTIVITIES
      { path: '/activities-home', name: 'activities-home', component: () => import('pages/activities/home/HomePage.vue') },
      { path: '/projects', name: 'projects', component: () => import('pages/activities/projects/ProjectsPage.vue') },
      { path: '/conversations', name: 'conversations', component: () => import('pages/activities/conversations/ConversationsPage.vue') },
      { path: '/agreements', name: 'agreements', component: () => import('pages/activities/agreements/AgreementsPage.vue') },
      //CRM
      { path: '/schedules', name: 'schedules', component: () => import('pages/crm/schedules/SchedulesPage.vue') },
      { path: '/campaings', name: 'campaings', component: () => import('pages/crm/campaings/CampaingsPage.vue') },
      { path: '/contacts', name: 'contacts', component: () => import('pages/crm/contacts/ContactsPage.vue') },
      { path: '/oportunities', name: 'oportunities', component: () => import('pages/crm/oportunities/OportunitiesPage.vue') },
      { path: '/prices', name: 'prices', component: () => import('pages/crm/prices/PricesPage.vue') },
      //FINANZAS
      { path: '/registers', name: 'registers', component: () => import('pages/finances/registers/RegistersPage.vue') },
      { path: '/cash-flow', name: 'cash-flow', component: () => import('pages/finances/cash-flow/CashFlowPage.vue') },

    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
