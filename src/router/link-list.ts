import { EssentialLinkProps } from '../components/EssentialLink.vue';

export const essentialLinks: EssentialLinkProps[] = [
  {
    title: 'Typograpy',
    caption: 'Tipos de letras en Quasar',
    icon: 'las la-font',
    link: 'typography',
    modulo: 'Activities',
  },
  {
    title: 'Flex',
    caption: 'Estilos con flex',
    icon: 'las la-layer-group',
    link: 'flex',
    modulo: 'Activities',
  },
  {
    title: 'About',
    caption: 'acerca del proyecto',
    icon: 'lab la-forumbee',
    link: 'about',
    modulo: 'Activities',
  },
  {
    title: 'Docs',
    caption: 'quasar.dev',
    icon: 'las la-graduation-cap',
    link: 'https://quasar.dev',
    modulo: 'Activities',
  },
];

export const essentialLinksActivities: EssentialLinkProps[] = [
  //ACTIVITIES
  {
    title: 'HOME',
    caption: 'Pagina inicial',
    icon: 'las la-home',
    link: 'activities-home',
    modulo: 'Activities',
  },
  {
    title: 'PROYECTOS',
    caption: 'Lista de proyectos',
    icon: 'las la-project-diagram',
    link: 'projects',
    modulo: 'Activities'
  },
  {
    title: 'Proveedores',
    caption: 'Pagina inicial',
    icon: 'las la-home',
    link: 'logistic',
    modulo: 'Logística',
  },
  {
    title: 'Orden compra',
    caption: 'Pagina inicial',
    icon: 'las la-home',
    link: 'ordenCompra',
    modulo: 'Logística',
  },
  {
    title: 'Bodega',
    caption: 'Pagina inicial',
    icon: 'las la-home',
    link: 'almacen',
    modulo: 'Logística',
  },
  {
    title: 'Producto',
    caption: 'Pagina inicial',
    icon: 'las la-home',
    link: 'producto',
    modulo: 'Logística',
  },
  {
    title: 'CONVERSACIONES',
    caption: 'Chats de grupos',
    icon: 'lab la-rocketchat',
    link: 'conversations',
    modulo: 'Activities',
  },
  {
    title: 'ACUERDOS',
    caption: 'Lista de acuerdos',
    icon: 'las la-file-alt',
    link: 'agreements',
    modulo: 'Activities',
  },
  {
    title: 'AGENDA',
    caption: 'Lista de agendas',
    icon: 'las la-clock',
    link: 'schedules',
    modulo: 'CRM'
  },
  {
    title: 'CAMPAÑAS',
    caption: 'Lista de acuerdos',
    icon: 'las la-bell',
    link: 'campaings',
    modulo: 'CRM'
  },
  {
    title: 'CONTACTOS',
    caption: 'Lista ',
    icon: 'las la-user-friends',
    link: 'contacts',
    modulo: 'CRM'
  },
  {
    title: 'OPORTUNIDADES',
    caption: 'Lista ',
    icon: 'las la-lightbulb',
    link: 'oportunities',
    modulo: 'CRM'
  },
  {
    title: 'COTIZACION',
    caption: 'Lista ',
    icon: 'las la-calculator',
    link: 'prices',
    modulo: 'CRM'
  },
  {
    title: 'REGISTROS',
    caption: 'Lista ',
    icon: 'las la-list',
    link: 'registers',
    modulo: 'Finanzas'
  },
  {
    title: 'FLUJO DE CAJA',
    caption: 'Lista ',
    icon: 'las la-coins',
    link: 'cash-flow',
    modulo: 'Finanzas'
  },
  //OTHERS
  {
    title: 'Typograpy',
    caption: 'Tipos de letras en Quasar',
    icon: 'las la-font',
    link: 'typography',
    modulo: 'Others'
  },
  {
    title: 'Flex',
    caption: 'Estilos con flex',
    icon: 'las la-layer-group',
    link: 'flex',
    modulo: 'Others'
  },
  {
    title: 'About',
    caption: 'acerca del proyecto',
    icon: 'lab la-forumbee',
    link: 'about',
    modulo: 'Others'
  },
  {
    title: 'Docs',
    caption: 'quasar.dev',
    icon: 'las la-graduation-cap',
    link: 'https://quasar.dev',
    modulo: 'Others'
  },
];
